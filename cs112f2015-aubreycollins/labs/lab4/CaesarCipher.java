class CaesarCipher {

	public static String encrypt(String input, int offset) {
		String output = "";

		for (int i = 0; i < input.length(); i++) {
			char c = input.charAt(i);
			output += (char) (c + overflowOffset(offset));
		}

		return output;
	}

	public static String decrypt(String input, int offset) {
		String output = "";

		for (int i = 0; i < input.length(); i++) {
			char c = input.charAt(i);
			output += (char) (c - overflowOffset(offset));
		}
		return output;
	}

	public static int overflowOffset(int num) {
		//allow letters to overflow
		//0-25 instead of 1-26
		while (num >= 25) {
			num -= 25;
		}
		return num;
	}

	public static void main(String[] args) {
		String encrypted = encrypt("SAMPLE", 26);
		System.out.println("Encrypting 'SAMPLE' with offset 26: " + encrypted);
		String decrypted = decrypt(encrypted, 26);
		System.out.println("Decrypting with offset 26: " + decrypted);
	}

}
