class SelectionSort {

	public static int[] sort(int[] array) {
		int sortedArray[] = array;
		int minPosition = 0;

		for (int i = 0; i < array.length; i++) {
			//reset minimum position;
			//we're only looking for values to the right of i
			minPosition = i;
			
			//find the smallest value to the right of i
			for (int x = i; x < array.length; x++) {
				if (sortedArray[x] < sortedArray[minPosition]) {
					minPosition = x;
				}
			}	
			
			//swap i and smallest
			int temp = sortedArray[i];
			sortedArray[i] = sortedArray[minPosition];
			sortedArray[minPosition] = temp;

			//print the array
			String output = "";
			for (int x = 0; x < sortedArray.length; x++) {
				output = output + sortedArray[x] + " ";
			}
			System.out.println(output);
		}

		return sortedArray;
	}

	public static void main(String[] args) {
		int sampleArray[] = {10, 5, 2, 6, 0, 20, 100, 50, 22, -999};
		int sortedArray[] = sort(sampleArray);
	}

}
