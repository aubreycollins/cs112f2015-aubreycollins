##Git and BitBucket

BitBucket must first be set up by adding an SSH key to your account. Without doing this, it's not possible to push changes to the repository. You can then use `git init` in the desired directory, and create a BitBucket repository with a matching name. `git add` can then be used to add the desired files, followed by `git commit` to commit the changes to the repository. `git push -u origin master` synchronizes the changes with the matching BitBucket repository.

####Git Commands
 
 - `git init [directory]`:  creates a new Git repository in an existing directory
 - `git status`:  displays the status of the current Git directory; tracked/untracked  files and file changes
 - `git add [file]`:  adds a file's changes to the next commit
 - `git commit`:  commits staged changes in the tracked files to the repository
 - `git push`: pushes the changes in a commit to a remote server
 - `git pull`: modifies the local copy of the repository according to the changes from the remote server

##Vim Overview

####Runtime Configuration Files

Vim uses a runtime configuration file, .vimrc, to give users extra control over the functionality of the editor, including preferences such as key bindings and color scheme. The runtime configuration file also allows for the use of plugins to further configure Vim. Vim uses its internal scripting language, Vimscript, for .vimrc as well as other configuration files.

####Basic Vim Features

Vim has several different modes, including normal, insert, and visual modes. Vim starts in normal mode by default. In this mode you can run commands, prefixed by a colon. To reenter normal mode from another mode, press `esc`. Insert mode is used for text entry, and can be entered by pressing `i`. Visual mode is used for selecting and manipulating text. It can be entered through mouse input, or by pressing `v`.

####Plugins

 - **Ctrl-P**: Finds files. Enter `control + p` to bring up the file navigator, then type to search for matches for a specific input.
 - **Fugitive**: Integrates Git commands into Vim. View status with `:Gstatus`, and press `-` to stage the file's changes for commit. Use `:Gcommit` to commit.
 - **MatchIt**: Pressing `%` matches grouped symbols such as parentheses and brackets.
 - **NerdTree**: File browser. Press `F11` to bring up the file tree.
 - **Sneak**: Jumps to a particular spot indicated by a two-character search. press `s` followed by any two characters to search for a match, and press `;` to jump to the next match. Use `S` for backwards searches.
 - **TComment**: Comments/uncomments lines of code. Press `gcc` to comment the current line.
 - **Tagbar**: Displays a menu showing the basic structure of a code file. Use `:TagbarToggle` to open.
