###Lab 1 Procedure

First, I used `ssh-keygen` to generate an SSH key. I added this key to my BitBucket account so that I could use Git to synchronize local and remote repositories. I used `git clone` to make a copy of the `jwenskovitch/cs112f2015-share` repository on the local machine. I created a new directory called `cs112f2015-aubreycollins` and used `git init` to initialize a new Git repository in that directory. 

I created a `labs` directory and used `cp` to copy the necessary lab source code into `cs112f2015-aubreycollins/labs/lab1/src`. I used `git add *` to stage all the files in the directory for commit. Then I used `git commit` followed by `git push -u origin master` to update the BitBucket repository according to my local changes.

I configured vim by copying `.vimrc` and `.gvimrc` from `lab1/src/dotfiles` to `~/`, as well as `lab1/src/colors/Monokai.vim` to `~/.vim/colors`. I modified my `.vimrc` file to use Monokai as the default color scheme. I ran `:BundleInstall` to install all of the plugins specified in `.vimrc`.

I used `javac [filename].java` to compile Hooray.java and Weeee.java, followed by `java [filename]` to run them. Both programs are infinite loops, printing "Hooray!" and "Weeee!" respectively on each iteration. Weeee.java, however, uses recursion instead of a while loop as used in Hooray.java, so it eventually runs out of memory and crashes.
