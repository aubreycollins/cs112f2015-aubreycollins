public class Weeee {

    public static void weeee() {
		System.out.println("Weeee!"); //prints "Weeee!"
		weeee(); //recursively calls itself
    } //weeee

    public static void main(String[] args) {
		weeee(); //call weeee when program runs
    } //main

} //Weeee (class)
