public class Hooray {

    public static void hooray() {

		while(true) { //infinite loop; true == true
			System.out.println("Hooray!"); //prints "Hooray!" on every loop
    	} //while

    } //hooray

    public static void main(String[] args) {
		hooray(); //call hooray when program runs
    } //main

} //Hooray (class)
