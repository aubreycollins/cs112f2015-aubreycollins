import java.util.Scanner;
import java.util.Random;

class GuessingGame {

	public static void main(String[] args) {
		boolean playAnotherRound;
		int min = 1;
		int max = 100;
		while (true) {
			// loop until user doesn't want to play another round
			playRound(min, max);
			playAnotherRound = playAgain();
			if (playAnotherRound) {
				continue;
			} else {
				break;
			}
		}
	}

	public static void playRound(int min, int max) {
		int number = generateNumber(min, max);
		int guessComparison = 1;
		int guess;
		int tries = 0;
		while (true) {
			// loop until user guesses the number
			tries++;
			guess = getGuess(min, max);
			guessComparison = checkGuess(guess, number);
			// 0: match; 1: too high; -1: too low
			if (guessComparison == -1) {
				System.out.println(guess + " is too low.");
			} else if (guessComparison == 1) {
				System.out.println(guess + " is too high.");
			} else if (guessComparison == 0) {
				System.out.println(guess + " is correct! It took you " + tries + " tries.");
				break;
			} else {
				System.out.println("Something broke");
			}
		}
	}

	public static boolean playAgain() {
		System.out.println("Would you like to play again? (Y/N)");
		Scanner scan = new Scanner(System.in);	
		while (true) {
			// loop until user enters Y or N
			String userInput = scan.nextLine().toUpperCase();
			if (userInput.equals("Y")) {
				return true;
			} else if (userInput.equals("N")) {
				return false;
			} else {
				System.out.println("Please enter Y or N.");
			}
		}
	}

	public static int generateNumber(int min, int max) {
		Random rand = new Random();
		int randomNumber = rand.nextInt((max - min) + 1) + min;
		return randomNumber;
	}

	public static int getGuess(int min, int max) {
		Scanner scan = new Scanner(System.in);
		String userInput; 
		System.out.println("Enter your guess:");
		int guess;
		// loop until user enters a valid number, then return it
		while (true) {
			try {
				userInput = scan.nextLine().toUpperCase();
				guess = Integer.parseInt(userInput);
				if (guess >= min && guess <= max) { //within valid range
					return guess;
				} else {
					System.out.println("Number must be between " + min + " and " + max + ".");
					continue;
				}
			} catch(NumberFormatException e) {
				// in case userInput can't be converted to an int
				System.out.println("Please enter a valid number.");
			}
		}
	}

	public static int checkGuess(int guess, int actual) {
		// 0: match; -1: guess too low; 1: guess too high
		if (guess == actual) {
			return 0;
		} else if (guess > actual) {
			return 1;
		} else {
			return -1;
		}
	}

}
