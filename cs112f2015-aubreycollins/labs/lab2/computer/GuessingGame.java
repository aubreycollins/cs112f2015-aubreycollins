import java.util.Scanner;

class GuessingGame {

	public static void main(String[] args) {
		boolean playAnotherRound;
		int min = 1;
		int max = 100;
		while (true) {
			// loop until user doesn't want to play another round
			playRound(min, max);
			playAnotherRound = playAgain();
			if (playAnotherRound) {
				continue;
			} else {
				break;
			}
		}
	}

	public static void playRound(int min, int max) {
		int guessComparison = 1;
		int guess;
		int tries = 0;
		int upperBound = max;
		int lowerBound = min;
		System.out.println("Think of a number between " + min + " and " + max + ".");
		while (true) {
			tries++;
			guess = makeGuess(upperBound, lowerBound);
			System.out.println("My guess is " + guess + ".");
			guessComparison = checkGuess(guess);
			// adjust bounds according to feedback
			if (guessComparison == -1) {
				lowerBound = guess;
			} else if (guessComparison == 1) {
				upperBound = guess;
			} else if (guessComparison == 0) {
				System.out.println("I guessed it in " + tries + " tries.");
				break;
			} else {
				System.out.println("Something unexpected happened.");
			}
		}
	}

	public static int makeGuess(int upperBound, int lowerBound) {
		// makes a guess in between the upper and lower bound
		int average = (int) Math.floor((upperBound + lowerBound) / 2);
		return average;
	}

	public static int checkGuess(int guess) {
		Scanner scan = new Scanner(System.in);
		String userInput;
		// ask user for feedback and return it
		System.out.println("Enter -1 if my guess is too low, 0 if it's right, or 1 if it's too high.");
		while (true) {
			// loop until user enters valid feedback
			userInput = scan.nextLine();
			if (userInput.equals("-1")) {
				return -1;
			} else if (userInput.equals("0")) {
				return 0;
			} else if (userInput.equals("1")) {
				return 1;
			} else {
				System.out.println("Please enter -1, 0, or 1.");
				continue;
			}
		}
	}

	public static boolean playAgain() {
		System.out.println("Would you like to play again? (Y/N)");
		Scanner scan = new Scanner(System.in);	
		while (true) {
			// loop until user enters Y or N
			String userInput = scan.nextLine().toUpperCase();
			if (userInput.equals("Y")) {
				return true;
			} else if (userInput.equals("N")) {
				return false;
			} else {
				System.out.println("Please enter Y or N.");
			}
		}
	}

}
