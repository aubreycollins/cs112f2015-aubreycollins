### 1. Describe at an algorithmic level (no code required) how a Java method can count the number of vowels in a provided String.

Loop from 0 to the String's length - 1. On each iteration, increment the vowel count if the character at that index is A, E, I, O or U.

### 2. Describe at an algorithmic level how a Java method can take a provided integer n and return the sum of all positive, odd integers less than n.

Loop through numbers less than n. On each iteration, check to see if it's greater than 0 and odd (i.e., it has a nonzero remainder when divided by 2). If so, add it to sum of the previous numbers.

### 3. Describe at an algorithmic level how a Java method can take an array of provided integers and determine whether or not they are all distinct.

Loop through the array. On each iteration, loop through every index less than the current number to determine if there is redundancy.
