##### Name: 
Aubrey Collins 
##### Assignment: 
Lab 2
##### Program Due Date: 
9/9
##### Handed in Date: 
9/9
##### JDK Version: 
1.8.0_60
##### Source code file name(s):
./human/GuessingGame.java, ./computer/GuessingGame.java
##### Compiled (.jar or .class) file name(s): 
./human/GuessingGame.class, ./computer/GuessingGame.class
##### Does your program compile without error?:  
Yes
##### If not, what is/are the error(s)?:
N/A
##### Does your program run without error?:
Yes
##### If not, what is/are the error(s) and which parts of your program run correctly?:
N/A
##### Additional comments (including problems and extra credit):
Problems in lab2_questions.md
