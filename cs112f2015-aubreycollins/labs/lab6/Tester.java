import java.util.LinkedList;

class Tester {

	public static void main(String[] args) {
		int iterations[] = {1000, 10000, 50000, 100000};
		int testsPerIteration = 5;
		
		String s = new String();
		Test<String> stringTest = new Test<String>(s);

		StringBuilder sb = new StringBuilder();
		Test<StringBuilder> stringBuilderTest = new Test<StringBuilder>(sb);
		
		int a[] = new int[0];
		Test<int[]> arrayTest = new Test<int[]>(a); //workaround
		
		LinkedList l = new LinkedList();
		Test<LinkedList> linkedListTest = new Test<LinkedList>(l);
		
		SinglyLinkedList sl = new SinglyLinkedList();
		Test<SinglyLinkedList> singlyLinkedListTest = new Test<SinglyLinkedList>(sl);

		System.out.println("Testing appending: ");
		for (int i = 0; i < iterations.length; i++) {
			long stringAvg = stringTest.avgManyTests(testsPerIteration, iterations[i]);
			long stringBuilderAvg = stringBuilderTest.avgManyTests(testsPerIteration, iterations[i]);
			long arrayAvg = arrayTest.avgManyTests(testsPerIteration, iterations[i]);
			long linkedListAvg = linkedListTest.avgManyTests(testsPerIteration, iterations[i]);
			long singlyLinkedListAvg = singlyLinkedListTest.avgManyTests(testsPerIteration, iterations[i]);
			
			System.out.println(iterations[i] + " String: " + stringAvg);
			System.out.println(iterations[i] + " StringBuilder: " + stringBuilderAvg);
			System.out.println(iterations[i] + " Array: " + arrayAvg);
			System.out.println(iterations[i] + " LinkedList: " + linkedListAvg);
			System.out.println(iterations[i] + " SinglyLinkedList: " + singlyLinkedListAvg);
		}

		System.out.println("Testing retrieval: ");
		arrayTest.retrieval = true;
		linkedListTest.retrieval = true;
		singlyLinkedListTest.retrieval = true;
		for (int i = 0; i < iterations.length; i++) {
			long arrayAvg = arrayTest.avgManyTests(testsPerIteration, iterations[i]);
			long linkedListAvg = linkedListTest.avgManyTests(testsPerIteration, iterations[i]);
			long singlyLinkedListAvg = singlyLinkedListTest.avgManyTests(testsPerIteration, iterations[i]);

			System.out.println(iterations[i] + " Array: " + arrayAvg);
			System.out.println(iterations[i] + " LinkedList: " + linkedListAvg);
			System.out.println(iterations[i] + " SinglyLinkedList: " + singlyLinkedListAvg);
		}
	}

	private static class Test<KEY> {

		public KEY testStringType;
		public boolean retrieval = false;

		public Test(KEY s) {
			testStringType = s;
		}

		public long avgManyTests(int numTests, int loops) {
			long sum = 0;
			for (int i = 0; i < numTests; i++) {
				sum += run(loops);
			}
			return sum / numTests;
		}

		public long run(int loops) {
			long startTime = System.nanoTime();
			if (testStringType instanceof int[]) {
				int testArray[] = new int[loops];
				for (int i = 0; i < loops; i++) {
					testArray[i] = i;
				}
				if (retrieval) {
					startTime = System.nanoTime();
					for (int i = 0; i < loops; i++) {
						int temp = testArray[i];
					}
				}
			} else if (testStringType instanceof LinkedList) {
				LinkedList<Integer> testList = new LinkedList<Integer>();
				for (int i = 0; i < loops; i ++) {
					testList.add(0);
				}
				if (retrieval) {
					startTime = System.nanoTime();
					for (int i = 0; i < loops; i++) {
						int temp = testList.get(i);
					}
				}
			} else if (testStringType instanceof SinglyLinkedList) {
				SinglyLinkedList<Integer> testList = new SinglyLinkedList<Integer>();
				for (int i = 0; i < loops; i ++) {
					testList.add(0);
				}
				if (retrieval) {
					startTime = System.nanoTime();
					for (int i = 0; i < loops; i++) {
						int temp = testList.get(i);
					}
				}
			} else if (testStringType instanceof String) {
				String testString = new String();
				for (int i = 0; i < loops; i++) {
					testString = testString + "a";
				}
			} else if (testStringType instanceof StringBuilder) {
				StringBuilder testString = new StringBuilder();
				for (int i = 0; i < loops; i++) {
					testString.append("a");
				}
			} else {
				System.out.println("Invalid type for test object");
				System.exit(1);
			}
			long endTime = System.nanoTime();
			return endTime - startTime;
		}

	}

}
