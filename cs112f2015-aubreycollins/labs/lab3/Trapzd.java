class Trapzd extends Quad {

	private double otherBase;
	private double side1;
	private double side2;

	public Trapzd(double base1, double height, double base2, double side1Val, double side2Val) {
		otherBase = base2;
		side1 = side1Val;
		side2 = side2Val;
		setWidth(base1);
		setHeight(height);
	}

	public void calculateArea() {
		setArea((getHeight() * ((getWidth() + otherBase) / 2)));
	}
	
	public void calculatePerimeter() {
		setPerimeter(getWidth() + otherBase + side1 + side2);
	}

}
