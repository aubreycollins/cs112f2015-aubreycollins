class Line extends Shape {
	
	private double x1;
	private double x2;
	private double y1;
	private double y2;

	public Line(double x1Val, double y1Val, double x2Val, double y2Val) {
		x1 = x1Val;
		y1 = y1Val;
		x2 = x2Val;
		y2 = y2Val;
	}

	public void calculateArea() {
		setArea(0);	
	}

	public void calculatePerimeter() {
		double p = 2 * Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
		setPerimeter(p);
	}

}
