class Quad extends Shape {

	private double width;
	private double height;

	public void setWidth(double w) {
		width = w;
	}
	
	public double getWidth() {
		return width;
	}

	public void setHeight(double h) {
		height = h;
	}

	public double getHeight() {
		return height;
	}

}
