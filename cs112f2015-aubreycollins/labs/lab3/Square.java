class Square extends Rect {

	public Square(double w) {
		super(w, w);
	}

	public void calculateArea() {
		setArea(Math.pow(getWidth(), 2));
	}

	public void calculatePerimeter() {
		setPerimeter(4 * getWidth());
	}
}
