class Rect extends Quad {

	public Rect(double w, double h) {
		setWidth(w);
		setHeight(h);
	}

	public void calculateArea() {
		setArea(getWidth() * getHeight());
	}

	public void calculatePerimeter() {
		setPerimeter(2 * (getWidth() + getHeight()));
	}

}
