class Shape {

	private double area;
	private double perimeter;

	public double getArea() {
		return area;
	}

	public void setArea(double a) {
		area = a;
	}
	
	public double getPerimeter() {
		return perimeter;
	}

	public void setPerimeter(double p) {
		perimeter = p;
	}

}
