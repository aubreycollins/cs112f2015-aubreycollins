class Circle extends Round {

	private double radius;

	public Circle(double r) {
		super();
		radius = r;
	}

	public void calculatePerimeter() {
		setPerimeter(2 * getPi() * radius);
	}

	public void calculateArea() {
		setArea(getPi() * Math.pow(radius, 2));
	}

}
