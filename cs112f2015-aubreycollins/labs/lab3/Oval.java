class Oval extends Round {

	private double major;
	private double minor;

	public Oval(double maj, double min) {
		super();
		major = maj;
		minor = min;
	}

	public void calculateArea() {
		double a = getPi() * major * minor;
		setArea(a);
	}

	public void calculatePerimeter() {
		double temp = (Math.pow(major, 2) + Math.pow(minor, 2)) / 2;
		double p = 2 * getPi() * Math.sqrt(temp);
		setPerimeter(p);
	}

}
