class BST {

	private class BTreeNode {
		private int value;
		private BTreeNode parent;
		private BTreeNode leftChild;
		private BTreeNode rightChild;

		public BTreeNode(int v, BTreeNode p, BTreeNode l, BTreeNode r) {
			value = v;
			parent = p;
			leftChild = l;
			rightChild = r;
		} //BTreeNode (constructor)

		public int getValue() {
			return value;
		} //getValue

		public void setValue(int v) {
			value = v;
		} //set Value

		public BTreeNode getParent() {
			return parent;
		} //getParent

		public void setParent(BTreeNode p) {
			parent = p;
		} //setParent

		public BTreeNode getLeftChild() {
			return leftChild;
		} //getLeftChild

		public void setLeftChild(BTreeNode l) {
			leftChild = l;
		} //setLeftChild

		public BTreeNode getRightChild() {
			return rightChild;
		} //getRightChild

		public void setRightChild(BTreeNode r) {
			rightChild = r;
		} //setRightChild

	} //BTreeNode (internal class)


	// start of BST class
	BTreeNode root;
	int size;

	public BST() {
		root = null;
		size = 0 ;
	} //BST (constructor)

	public int size() {
		return size;
	} //size)

	public boolean isEmpty() {
		if (size == 0) {
			return true;
		} else {
			return false;
		} //if-else
	} //isEmpty


	/*
	** This is the setup function for insert
	** It takes a value as input, and calls the recursive insert function starting at the root
	*/
	public void insert(int value) {
		if (root == null) {
			BTreeNode newNode = new BTreeNode(value, null, null, null);	
			root = newNode;
		} else {
			insert(value, root);
		}	
	} //add (setup)

	/*
	** This is the recursive function for insert
	** It takes a value and a BTreeNode as input, and either adds a node at the current location
	** (if null), or travels to the left or right child to try to insert there
	*/
	public void insert(int value, BTreeNode currentLocation) {
		BTreeNode left = currentLocation.getLeftChild();
		BTreeNode right = currentLocation.getRightChild();
		if (value >= currentLocation.getValue()) {
			if (right != null) {
				insert(value, right);
			} else {
				BTreeNode newNode = new BTreeNode(value, currentLocation, null, null);	
				currentLocation.setRightChild(newNode);
			}
		} else {
			if (left != null) {
				insert(value, left);
			} else {
				BTreeNode newNode = new BTreeNode(value, currentLocation, null, null);	
				currentLocation.setLeftChild(newNode);			}
		}
	} //add (recursive)



	/*
	** This is the setup function for get
	** It takes a value as input, and calls the recursive get function starting at the root
	**
	** If the value isn't found, let's return a -1
	*/
	public int get(int value) {
		if (root == null) {
			return -1;
		}
		return get(value, root);
	} //get (setup)

	/*
	** This is the recursive function for get
	** It takes a value and a BTreeNode as input, and either finds a node at the current location
	** (if value=currentLocation.getValue()), or travels to the left or right child to try to find it there
	**
	** If the value isn't found, let's return a -1
	*/
	public int get(int value, BTreeNode currentLocation) {
		BTreeNode left = currentLocation.getLeftChild();
		BTreeNode right = currentLocation.getRightChild();
		int currentVal = currentLocation.getValue();
		if (currentVal == value) {
			return currentVal;	
		} else {
			if (value >= currentLocation.getValue()) {
				if (right != null) {
					return get(value, right);
				}
			} else {
				if (left != null) {
					return get(value, left);
				}
			}
		}
		return -1;
	} //get (recursive)



	/*
	** This is the setup function for remove
	** It takes a value as input, and calls the recursive remove function starting at the root
	*/
	public void remove(int value) {
		remove(value, root);
	} //remove (setup)

	/*
	** This is the recursive function for remove
	** It takes a value and a BTreeNode as input, and either removes the node at the current location
	** (if value=currentLocation.getValue()), or travels to the left or right child to try to remove from there
	*/
	public void remove(int value, BTreeNode currentLocation) {
		int v = currentLocation.getValue();
		BTreeNode l = currentLocation.getLeftChild();
		BTreeNode r = currentLocation.getRightChild();
		
		if (value == v) {
			// delete current node
			System.out.println("Found node");
			BTreeNode p = currentLocation.getParent();		
			if (l != null && r == null) {
				//has left
				l.setParent(p);
				p.setLeftChild(l);
			} else if (r != null && l == null) {
				//has right
				r.setParent(p);
				p.setRightChild(r);
			} else if (r != null && l != null) {
				//has both
			} else {
				//no children
				if (p.getLeftChild().getValue() == value) {
					//current is left child
					p.setLeftChild(null);
				} else {
					//current is right child
					p.setRightChild(null);
				}
			}
		} else {
			//look for the deletion node
			if (value >= v) {
				System.out.println("Searching right");
				remove(value, r);
			} else {
				System.out.println("Searching left");
				remove(value, l);
			}
		}
	} //remove (recursive)

} //BST
