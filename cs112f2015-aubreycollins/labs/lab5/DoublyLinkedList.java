class DoublyLinkedList {

	private static class Node {

		private int data;
		private Node next;
		private Node previous;

		public Node() {
			data = 0;
			next = null;
			previous = null;
		} //Node (constructor)

		public Node(int myData, Node newNext, Node newPrevious) {
			data = myData;
			next = newNext;
			previous = newPrevious;
		} //Node (constructor)

		public int getData() {
			return data;
		} //getValue

		public void setData(int myData) {
			data = myData;
		} //setValue

		public Node getNext() {
			return next;
		} //getNext

		public void setNext(Node newNext) {
			next = newNext;
		} //setNext

		public boolean hasNext() {
			if (next != null) {
				return true;
			} else {
				return false;
			}
		}

		public Node getPrevious() {
			return previous;
		}

		public void setPrevious(Node newPrevious) {
			previous = newPrevious;
		}

	} //Node (inner class)



	// ------------------------------

	private Node head;
	private Node tail;
	private int size;

	public DoublyLinkedList() {
		head = null;
		tail = null;
		size = 0;
	} //DoublyLinkedList (constructor)

	public int size() {
		return size;
	} //size

	public void add(int myData) {
		Node newNode = new Node(myData, null, null);
		if (head == null) {
			//empty list
			head = newNode;
			tail = newNode;
		} else {
			tail.setNext(newNode);
			newNode.setPrevious(tail);
			tail = newNode;
		}
		size++;
	} //add

	public int get(int index) {
		if (index < 0) {
			System.exit(1);

		} else if (index >= size) {
			System.exit(2);

		} else {
			int i = 0;
			Node temp = head;
			while (i != index) {
				i++;
				temp = temp.getNext();
			} //while

			return temp.getData();

		} //if-else

		return -1;

	} //get

	public int getFromEnd(int index) {
		if (index < 0) {
			System.exit(1);
		} else if (index >= size) {
			System.exit(2);
		} else {
			Node current = tail;
			for (int i = size-1; i != index; i--) {
				current = current.getPrevious();
			}
			return current.getData();
		}
		return -1;
	} //getFromEnd

	public void remove(int index) {
		if (index >= size) {
			System.exit(1);
		} else if (index < 0) {
			System.exit(2);
		} else if (index == 0) {
			if (head.hasNext()) {
				Node newHead = head.getNext();
				newHead.setPrevious(null);
				head.setNext(null);
				head = newHead; 
				size--;
			} else {
				// list is just the head
				head = null;
				size--;
			}
		} else {
			Node prev = head;
			Node next = prev.getNext();
			if (!next.hasNext()) {
				// short list; in this case, next is actually the one being removed
				prev.setNext(null);
				size--;
			} else {
				next = next.getNext();
				for (int i = 1; i != index; i++) {
					if (next.hasNext()) {
						prev = prev.getNext();
						next = next.getNext();
					} else {
						// at tail
						break;
					}
				}
			prev.setNext(next);
			next.setPrevious(prev);
			size--;
			}
		}
	} //remove

	public void circularize() {
		head.setPrevious(tail);
		tail.setNext(head);
	}

	public void testCircularization() {
		Node current = head;
		for (int i = 0; i < size * 3; i++) {
			System.out.println(current.getData());
			current = current.getNext();
		}
	}

	public boolean isEmpty() {
		if (size == 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean testPreviousLinks() {
		Node current = tail;
		int count = 1;
		while (current.getPrevious() != null) {
			current = current.getPrevious();
			count++;
		} //while
		return ((current == head) && (count == size));
	} //testPreviousLinks

} //DoublyLinkedList (class)
