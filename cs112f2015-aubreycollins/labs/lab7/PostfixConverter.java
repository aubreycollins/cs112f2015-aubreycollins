import java.util.*;

class PostfixConverter {
	
	private Stack<Character> stack;

	public String convert(String s) {
		String result = "";
		stack = new Stack<Character>();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c == '(') {
				stack.push(c);	
			} else if (c == ')') {
				while (!stack.isEmpty()) {
					char top = stack.pop();
					if (top != '(') {
						result += top;
					} else {
						break;
					}
				}
			} else if (c == '+' || c == '-') {
				try {
					char top = stack.peek();
					if (top == '+' || top == '-' || top == '*' || top == '/') {
						result += stack.pop();
					}
				} catch (EmptyStackException e) {
				} finally {
					stack.push(c);
				}	
			} else if (c == '*' || c == '/') {
				try {
					char top = stack.peek();
					if (top == '*' || top == '/') {
						result += stack.pop();
					}
				} catch (EmptyStackException e) {
				} finally {
					stack.push(c);
				}
			} else {
				result += c;
			}
		}
		try {
			char top = stack.peek();
			if (top == '(') {
				//ERROR
			} else if (top == '+' || top == '-' || top == '*' || top == '/') {
				while (!stack.isEmpty()) {
					result += stack.pop();
				}
			}
		} catch (EmptyStackException e) {
		
		}
		for (int i = 0; i < stack.size(); i++) {
			System.out.println(stack.pop());
		}
		
		return result;
	}

	public static void main(String[] args) {
		PostfixConverter conv = new PostfixConverter();
		String[] testStrings = {
			"a*b",
			"a/b+c",
			"(a+b)/c+d",
			"(a+b)/(c+d)",
			"((a-b)*c)/d",
		};
		for (int i = 0; i < testStrings.length; i++) {
			System.out.println(conv.convert(testStrings[i]));
		}
	}

}
