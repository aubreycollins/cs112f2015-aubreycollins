import java.util.*;

class Calculator {

	public static PostfixConverter conv = new PostfixConverter();
	public static Stack<Double> s = new Stack<Double>();

	public static double calculate(String input) {
		String converted = conv.convert(input);
		System.out.println("Converted " + input + " to " + converted);
		for (int i = 0; i < converted.length(); i++) {
			char c = converted.charAt(i);
			if (c == '+') {
				double operand1 = s.pop();
				double operand2 = s.pop();
				double result = operand1 + operand2;
				System.out.println("Adding top 2 items: " + result);
				s.push(result);
			} else if (c == '-') {
				double operand1 = s.pop();
				double operand2 = s.pop();
				double result = operand2 - operand1;
				System.out.println("Subtracting top 2 items: " + result);
				s.push(result);	
			} else if (c == '*') {
				double operand1 = s.pop();
				double operand2 = s.pop();
				double result = operand1 * operand2;
				System.out.println("Multiplying top 2 items: " + result);
				s.push(result);
			} else if (c == '/') {
				double operand1 = s.pop();
				double operand2 = s.pop();
				double result = operand2 / operand1;
				System.out.println("Dividing top 2 items: " + result);
				s.push(result);
			} else {
				System.out.println("Adding to stack: " + c);
				s.push((double) Character.getNumericValue(c));
			}
		}
		return s.pop();
	}

	public static void main(String args[]) {
		System.out.println(calculate("2*((9-5)/2)"));
	}

}
