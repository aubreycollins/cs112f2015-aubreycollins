class Player {

	protected int score;
	
	public void incrementScore() {
		score++;
	}

	public void setScore(int newVal) {
		score = newVal;
	}

	public int getScore() {
		return score;
	} 

} 
