import java.util.Scanner;

class Human extends Player {

	public Human() {
		setScore(0);
	} //Human (constructor)

	public String getInput() {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter your move (ALL CAPS PLEASE)");
		String userMove = scan.nextLine();
		return userMove;
	} //getInput

	public void helpHumanCheat() {
		setScore(4);
		System.out.println("Cheater.");
	}
} //Human
