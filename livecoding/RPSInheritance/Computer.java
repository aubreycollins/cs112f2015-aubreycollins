import java.util.Random;

class Computer extends Player {

	public Computer() {
		setScore(0);
	} //Computer (constructor)

	public void incrementScore() {
		setScore(getScore() + 2);
	}

	public String getInput() {
		Random rdm = new Random();
		int computerInput = rdm.nextInt(3);

		if (computerInput == 0) {
			return "ROCK";
		} else if (computerInput == 1) {
			return "PAPER";
		} else if (computerInput == 2) {
			return "SCISSORS";
		} else {
			return "ERROR";
		} //if-else	
	
	} //getInput

} //Human
