class RPSGame {

	private static int tieScore;
	private static int winsRequired;
	private static int roundWinner;

	private static Human humanPlayer;
	private static Computer computerPlayer;

	public static void main(String args[]) {
		humanPlayer = new Human();
		computerPlayer = new Computer();
		tieScore = 0;
		winsRequired = 5;

		do {
			// Play a round
			roundWinner = playRound();

			if (roundWinner == 0) {
				//userScore++;
				humanPlayer.incrementScore();
			} else if (roundWinner == 1) {
				//computerScore++;
				computerPlayer.incrementScore();
			} else if (roundWinner == 2) {
				tieScore++;
				//incrementScore();
			} else {
				System.exit(3);
			} //if-else

			//printScores(userScore, computerScore, tieScore);
			if (computerPlayer.getScore() == 4 && humanPlayer.getScore() < 4) {
				humanPlayer.helpHumanCheat();
			}
			printScores(humanPlayer.getScore(), computerPlayer.getScore(), tieScore);


		} while (humanPlayer.getScore() < winsRequired && computerPlayer.getScore() < winsRequired);

		printGameWinner(humanPlayer.getScore(), computerPlayer.getScore(), winsRequired);

	} //main

	public static int playRound() {
		String userInput;
		String computerInput;
		
		userInput = humanPlayer.getInput();
		computerInput = computerPlayer.getInput();
		
		roundWinner = pickRoundWinner(userInput, computerInput);

		if (roundWinner == 0) {
			System.out.println("USER WINS!!!");
		} else if (roundWinner == 1) {
			System.out.println("The computer has defeated you!");
		} else if (roundWinner == 2) {
			System.out.println("It's a tie, stop copying the computer!");
		} else {
			System.out.println("Something broke... I dunno");
		} //if-else

		return roundWinner;
	} //playRound

	// picking a round winner
	// return 0 = user wins
	// return 1 = computer wins
	// return 2 = tie
	// return 3 = error
	public static int pickRoundWinner(String u, String c) {
		if (u.equals(c)) {
			return 2;
		} else if (u.equals("ROCK") && c.equals("PAPER")) {
			return 1;
		} else if (u.equals("PAPER") && c.equals("ROCK")) {
			return 0;
		} else if (u.equals("ROCK") && c.equals("SCISSORS")) {
			return 0;
		} else if (u.equals("SCISSORS") && c.equals("ROCK")) {
			return 1;
		} else if (u.equals("PAPER") && c.equals("SCISSORS")) {
			return 1;
		} else if (u.equals("SCISSORS") && c.equals("PAPER")) {
			return 0;
		} else {
			System.out.println("something different broke");
			return 3;
		} //if-else
	} //pickRoundWinner

	public static void printScores(int userScore, int computerScore, int tieScore) {
		System.out.println("    User Score: " + userScore);
		System.out.println("Computer Score: " + computerScore);
		System.out.println("Number of Ties: " + tieScore);
		System.out.println();
	} //printScores

	public static void printGameWinner(int userScore, int computerScore, int wins) {
		if (computerScore >= wins) {
			System.out.println("The computer player has defeated you!");
		} else if (userScore <= wins) {
			System.out.println("Take that stupid computer!");
		} else {
			System.out.println("Bad things have happened here");
		} //if-else
	} //printGameWinner

	public static void incrementScore() {
		tieScore++;
	} //incrementScore

} //RPSGame (class)
