class MinMaxAvg {

	public static int min;
	public static int max;
	public static double sum;
	public static double avg;
	public static double totalLength;

	public static void main(String[] args) {
		int myArray[][] = {
			{7, 12, 4, -1},
			{-2, 3},
			{8, 9, 0, 1},
			{-12, 3, 4},
			{2}
		};

		min = myArray[0][0];
		max = myArray[0][0];
		sum = 0;
		avg = 0;
		totalLength = 0;

		for (int i = 0; i < myArray.length; i++) {
			for (int x = 0; x < myArray[i].length; x++) {
				totalLength++;
				sum += myArray[i][x];
				
				if (myArray[i][x] < min) {
					min = myArray[i][x];
				} else if (myArray[i][x] > max) {
					max = myArray[i][x];
				}
			}
		}

		avg = sum / totalLength;
		System.out.println("Min: " + min);
		System.out.println("Max: " + max);
		System.out.println("Avg: " + avg);
		System.out.println("Sum: " + sum);
		System.out.println("Total length: " + totalLength);
	}

}
