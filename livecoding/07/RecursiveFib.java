class RecursiveFib {

	public static void main(String[] args) {
		double fibGoal = 50;
		System.out.println(fib(fibGoal));
	}

	public static double fib(double n) {
		if (n == 0) {
			return 0;
		} else if (n == 1) {
			return 1;
		} else {
			return fib(n-1) + fib(n-2);
		}
	}

}
