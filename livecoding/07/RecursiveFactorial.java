class RecursiveFactorial {

	public static void main(String[] args) {
		double facNum = 12;
		double product = fact(facNum);
		System.out.println(product);
	}

	public static double fact(double n) {
		if (n == 1) {
			// base case
			return 1;
		} else {
			// recursive case
			return n * fact(n - 1);
		}
	}

}
