class IterativeFactorial {

	public static void main(String[] args) {
		int facNum = 12;
		int product = 1;
		for (int i = facNum; i >= 1; i--) {
			product *= i;
		}

		System.out.println(product);
	}

}
